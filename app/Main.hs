{-# LANGUAGE AllowAmbiguousTypes   #-}
{-# LANGUAGE GADTs                 #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE ParallelListComp      #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE StandaloneDeriving    #-}
{-# LANGUAGE TypeSynonymInstances  #-}

import           Control.Exception
import           Data.Array
import qualified Data.Text.IO                              as T
import           Data.Time
import qualified Data.Vector                               as V
import           DeepData.DeepData                         as D
import           System.IO
import           System.Random
import           Text.Printf

import           ActivationFunctions
import           Base                                      as B
import           Control.DeepSeq
import           General
import           Layers.FullyConnectedLayer

import qualified Data.ByteString.Lazy                      as BL
import           Data.Csv

import           BaseGPU

import           DeepData.Gpu.DeepGpu
import           Metrics.Metrics

import           LossFunctions

import           Layers.NeuralLayer.NeuralLayer


xx :: X
yy :: Y
xx = newDeepMatrix [0, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1] [4, 3]

yy = newDeepMatrix [0, 1, 1, 0] [4, 1]

main :: IO ()
main = do
  let dl = deepLearningTest
  print "PREDICTION"
  let !y1 = predict xx dl
  print deepLearningTest
  print y1


layers :: [Layers]
layers =
  [ neuralLayer 5 (makeStrdUpgrades sigmoid)
  , neuralLayer 8 (makeStrdUpgrades sigmoid)
  , neuralLayer 1 (makeStrdUpgrades sigmoid)
  ]

deepLearningTest = deepLearner xx yy layers 1000 4 differnce 45441



