{-# LANGUAGE DeriveGeneric #-}

module BaseTypes
  ( Layers(..)
  , Weights(..)
  , Alphas(..)
  , Deltas(..)
  , Gammas(..)
  , layerGenWt
  , layerFeedForward
  , layerBackprop
  , layerPull
  , layerFunc
  , layerUpdate
  , layerEnds
  , fullyConnected
  , dropout
  , neuralLayer
  ) where

import           ActivationFunctions                       as Act
import           DeepData.DeepData                         as D
import           General                                   as G
import           Layers.DropoutLayer                       as DP
import           Layers.FullyConnectedLayer                as FC
import           Layers.LayerClass                         as LC
import           Layers.NeuralLayer.NeuralLayer            as NL
import Layers.NeuralLayer.NeuralLayerTypes

import           Control.DeepSeq
import           GHC.Generics                              (Generic)

data Layers
  = FC FullyConnected
  | DP Dropout
  | NL NeuralLayer
  deriving (Generic, Show)

instance NFData Layers

-- data LayersTest a =  FC2 a deriving(Show)
type Weights = [Layers]

type Alphas = [Layers]

type Deltas = [Layers]

type Gammas = [Layers]

instance Layer Layers where
  genWt = layerGenWt
  feedForward = layerFeedForward
  backprop = layerBackprop
  pull = layerPull
  func = layerFunc
  update = layerUpdate
  ends = layerEnds

instance LayerGPU Layers where
  feedForwardGPU = layerFeedForwardGPU
  backpropGPU = layerBackpropGPU
  updateGPU = layerUpdateGPU

layerGenWt :: Layers -> Int -> Seed -> Layers
layerGenWt (FC a) b c = FC $ genWt a b c
layerGenWt (DP a) b c = DP $ genWt a b c
layerGenWt (NL a) b c = NL $ genWt a b c

layerFeedForward :: Layers -> K -> Layers
layerFeedForward (FC a) b = FC $ feedForward a b
layerFeedForward (DP a) b = DP $ feedForward a b
layerFeedForward (NL a) b = NL $ feedForward a b

layerBackprop :: Layers -> D -> D -> Layers
layerBackprop (FC a) b c = FC $ backprop a b c
layerBackprop (DP a) b c = DP $ backprop a b c
layerBackprop (NL a) b c = NL $ backprop a b c

layerPull :: Layers -> D
layerPull (FC a) = pull a
layerPull (DP a) = pull a
layerPull (NL a) = pull a

layerFunc :: Layers -> Activation
layerFunc (FC a) = func a
layerFunc (DP a) = func a
layerFunc (NL a) = func a

layerUpdate :: Layers -> D -> D -> Layers
layerUpdate (FC a) b c = FC $ update a b c
layerUpdate (DP a) b c = DP $ update a b c
layerUpdate (NL a) b c = NL $ update a b c

layerEnds :: Layers -> BaseLayer -> Layers
layerEnds (FC a) b = FC $ ends a b
layerEnds (DP a) b = DP $ ends a b
layerEnds (NL a) b = NL $ ends a b

layerFeedForwardGPU :: Layers -> K -> IO Layers
layerFeedForwardGPU (FC a) b = FC <$> feedForwardGPU a b

layerBackpropGPU :: Layers -> D -> D -> IO Layers
layerBackpropGPU (FC a) b c = FC <$> backpropGPU a b c

layerUpdateGPU :: Layers -> Layers -> D -> IO Layers
layerUpdateGPU (FC a) (FC b) c = FC <$> updateGPU a b c

fullyConnected :: Size -> Activation -> Double -> Layers
fullyConnected s a lr = FC (FullyConnected s (D [] []) a lr)

dropout :: Size -> Activation -> Double -> Layers
dropout s a lr = DP (Dropout $ FullyConnected s (D [] []) a lr)

neuralLayer :: Size -> Upgrades -> Layers
neuralLayer s ups = NL (NeuralLayer s (D [] []) ups)
