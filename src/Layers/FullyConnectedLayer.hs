{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE DeriveGeneric #-}
module Layers.FullyConnectedLayer
    (
        FullyConnected(..),
        genWt_,
        pull_,
        activation_,
        update_,
        feedForward_,
        backprop_,
        ends_
    ) where

import General as G
import DeepData.DeepData as D
import DeepData.Gpu.DeepGpu as D
import ActivationFunctions as Act
import Layers.LayerClass as LC
import Metrics.Metrics

import GHC.Generics (Generic)
import Control.DeepSeq

-- size data activation learningRate
data FullyConnected = FullyConnected Int D Activation Double deriving(Generic ,Show)
instance NFData FullyConnected
type AlphaFC = FullyConnected


instance Layer FullyConnected where
    genWt = genWt_
    feedForward = feedForward_
    backprop = backprop_
    pull = pull_
    func = activation_
    update = update_
    ends = ends_

instance LayerGPU FullyConnected where
    feedForwardGPU = feedForwardGPU_
    backpropGPU = backpropGPU_
    updateGPU = updateGPU_


-- instance LayerCore FullyConnected FullyConnected where
--     feedForward = feedForward_
--     backprop = backprop_



genWt_ :: FullyConnected -> Int -> Seed -> FullyConnected
genWt_ (FullyConnected i _ f lr) dim sd =

  FullyConnected i (newDeepRandomMatrix [dim, i] sd) f lr


feedForward_ :: FullyConnected -> K -> AlphaFC
feedForward_ (FullyConnected i h (Funcs base div s) lr) k =

  FullyConnected i (base (k *** h)) (Funcs base div s) lr


backprop_ :: AlphaFC -> D -> D -> FullyConnected
backprop_ (FullyConnected i k (Funcs act derivative s) lr) w delta =

  FullyConnected i
                 ((delta *** deepTranspose w) !*! derivative k)
                 (Funcs act derivative s)
                 lr


update_ :: FullyConnected -> D -> D -> FullyConnected
update_ (FullyConnected i w f lr) d k =

  FullyConnected i (w !+! lr |-> (deepTranspose k *** d)) f lr

activation_ :: FullyConnected -> Activation
activation_ (FullyConnected _ _ f _) = f

pull_ :: FullyConnected -> D
pull_ (FullyConnected _ d _ _) = d

ends_ :: FullyConnected -> BaseLayer -> FullyConnected
ends_ _ (BaseLayer a b c) = FullyConnected a b c 1


feedForwardGPU_ :: FullyConnected -> K -> IO AlphaFC
feedForwardGPU_ (FullyConnected i h (Funcs base div s) lr) k = do
  m <- k <***> h
  return $ FullyConnected i (base m) (Funcs base div s) lr


backpropGPU_ :: AlphaFC -> D -> D -> IO FullyConnected
backpropGPU_ (FullyConnected i k (Funcs act derivative s) lr) w delta = do
  m <- delta <***> deepTranspose w
  return $ FullyConnected i (m !*! derivative k) (Funcs act derivative s) lr


updateGPU_ :: FullyConnected -> FullyConnected -> D -> IO FullyConnected
updateGPU_ (FullyConnected i w f lr) (FullyConnected _ d _ _) k = do
  m <- deepTranspose k <***> d
  return $ FullyConnected i (w !+! m) f lr
