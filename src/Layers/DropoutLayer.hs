{-# LANGUAGE DeriveGeneric #-}
module Layers.DropoutLayer
    ( 
        Dropout(..),
        bernoulliDrop
    ) where

import Layers.FullyConnectedLayer as FC
import Layers.LayerClass as LC
import System.Random
import DeepData.DeepData as D
import General as G

import GHC.Generics (Generic)
import Control.DeepSeq

newtype Dropout = Dropout FullyConnected deriving(Generic, Show)
instance NFData Dropout

instance Layer Dropout where
    genWt (Dropout a) b c = Dropout $ genWt_ a b c
    pull (Dropout a)      = pull_ a
    func (Dropout a)      = activation_ a
    update (Dropout a) b c = Dropout $ update_ a b c
    feedForward = feedForwardDropout
    backprop (Dropout a) b c = Dropout $ backprop_ a b c
    ends (Dropout a) b = Dropout $ ends_ a b
    

bernoulli :: Int -> Int -> Int
bernoulli i p = bin (fst $ randomR (1,100) (mkStdGen i) :: Int)
    where
        bin :: Int -> Int
        bin a 
            | a >= p = 1
            | otherwise = 0 

bernoulliDrop :: D -> D
bernoulliDrop (D a b) = D (run a (bernoulli 34252 30) 0) b
    where
        size = last b
        run :: [Double] -> Int -> Int -> [Double]
        run [] _ _ = []
        run (x : xs) bernoul i
            | i `mod` size == 0 = x * fromIntegral bernoul : run xs (bernoulli (34252 * i) 30) (i + 1)
            | otherwise = x * fromIntegral bernoul : run xs bernoul (i + 1)


feedForwardDropout :: Dropout -> K -> Dropout
feedForwardDropout (Dropout (FullyConnected i d f lr)) k = 
                        Dropout $ feedForward_ (FullyConnected i (bernoulliDrop d) f lr) k



