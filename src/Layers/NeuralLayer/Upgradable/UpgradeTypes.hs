module Layers.NeuralLayer.Upgradable.UpgradeTypes
    (
        Upgradable(..)
    ) where

import           Control.DeepSeq
import           DeepData.DeepData                    as D
import           General                              as G
import           ActivationFunctions                       as Act

class Upgradable a where
--   genUpgrade :: a -> [Int] -> Seed -> Maybe a
  ffUpgrade :: a -> K -> D -> a
  bpUpgrade :: a -> D -> D -> D -> a
  upUpgrade :: a -> D -> D -> D -> a
  ffPullUpgrade :: a -> D
  pullActivation :: a -> Maybe Activation