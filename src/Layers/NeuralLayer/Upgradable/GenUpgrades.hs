module Layers.NeuralLayer.Upgradable.GenUpgrades
    (
        blankUpgrade
        , (<-||->)
        ,makeStrdUpgrades
        ,makeDropoutUpgrades
        , makeStdBiasUpgrades
    ) where

import Layers.NeuralLayer.NeuralLayerTypes
import           ActivationFunctions                       as Act
import           DeepData.DeepData                    as D
import           General                              as G
import Layers.NeuralLayer.Upgradable.Objects.StdUpgrade
import Layers.NeuralLayer.Upgradable.Upgrades
import Layers.NeuralLayer.Upgradable.Objects.DropoutUpgrade
import Layers.NeuralLayer.Upgradable.Objects.BiasUpgrade

infixl 8 <-||->

blankUpgrade :: Upgrades
blankUpgrade = Upgrades []

makeStrdUpgrades :: Activation -> Upgrades
makeStrdUpgrades a = Upgrades [STDR_1 (STANDARD_1 empty a)]

makeStdBiasUpgrades :: Activation -> Upgrades
makeStdBiasUpgrades a = Upgrades [STD_BIAS (STANDARD_BIASED_UPGRADE empty empty a)]

makeDropoutUpgrades :: Upgrades
makeDropoutUpgrades = Upgrades [DROPOUT (DROPOUT_UPGRADE empty)]

(<-||->) :: Upgrades -> Upgrades -> Upgrades
a <-||-> b = Upgrades $ (\(Upgrades x) (Upgrades y) -> x ++ y) a b