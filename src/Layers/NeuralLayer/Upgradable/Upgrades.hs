{-# LANGUAGE DeriveGeneric #-}
module Layers.NeuralLayer.Upgradable.Upgrades
    (
        ValidUpgrade(..)
    )where

import           Layers.NeuralLayer.Upgradable.UpgradeTypes
import           Layers.NeuralLayer.Upgradable.Objects.StdUpgrade
import Layers.NeuralLayer.Upgradable.Objects.DropoutUpgrade
import Layers.NeuralLayer.Upgradable.Objects.BiasUpgrade

import           Control.DeepSeq
import           DeepData.DeepData                    as D
import           General                              as G
import           GHC.Generics                             (Generic)
import           ActivationFunctions                       as Act

data ValidUpgrade =
  STDR_1 STANDARD_1 |
  DROPOUT DROPOUT_UPGRADE |
  STD_BIAS STANDARD_BIASED_UPGRADE
  deriving (Generic, Show)

instance NFData ValidUpgrade

instance Upgradable ValidUpgrade where
  -- genUpgrade = genUpgrades
  ffUpgrade = ffUpgrades
  bpUpgrade = bpUpgrades
  upUpgrade = upUpgrades
  ffPullUpgrade = ffPullUpgrades
  pullActivation = pullActivations

-- genUpgrades:: ValidUpgrade -> [Int] -> Seed -> Maybe ValidUpgrade
-- genUpgrades (STDR_1 a) b c = (\x -> case (x) of
--                                             Just a -> Just $ STDR_1 a
--                                             Nothing -> Nothing) (genUpgrade a b c)
-- genUpgrades (DROPOUT a) b c = (\x -> case (x) of
--                                             Just a -> Just $ DROPOUT a
--                                             Nothing -> Nothing) (genUpgrade a b c)
-- genUpgrades (STD_BIAS a) b c = (\x -> case (x) of
--                                             Just a -> Just $ STD_BIAS a
--                                             Nothing -> Nothing) (genUpgrade a b c)

ffUpgrades :: ValidUpgrade -> D -> K -> ValidUpgrade
ffUpgrades (STDR_1 a) d k = STDR_1 $ ffUpgrade a d k
ffUpgrades (DROPOUT a) d k = DROPOUT $ ffUpgrade a d k
ffUpgrades (STD_BIAS a) d k = STD_BIAS $ ffUpgrade a d k

bpUpgrades :: ValidUpgrade -> D -> D -> D -> ValidUpgrade
bpUpgrades (STDR_1 a) b c d = STDR_1 $ bpUpgrade a b c d
bpUpgrades (DROPOUT a) b c d = DROPOUT $ bpUpgrade a b c d
bpUpgrades (STD_BIAS a) b c d = STD_BIAS $ bpUpgrade a b c d

upUpgrades :: ValidUpgrade -> D -> D -> D -> ValidUpgrade
upUpgrades (STDR_1 a) b c d = STDR_1 $ upUpgrade a b c d
upUpgrades (DROPOUT a) b c d = DROPOUT $ upUpgrade a b c d
upUpgrades (STD_BIAS a) b c d = STD_BIAS $ upUpgrade a b c d

ffPullUpgrades :: ValidUpgrade -> D
ffPullUpgrades (STDR_1 a) = ffPullUpgrade a
ffPullUpgrades (DROPOUT a) = ffPullUpgrade a
ffPullUpgrades (STD_BIAS a) = ffPullUpgrade a

pullActivations :: ValidUpgrade -> Maybe Activation
pullActivations (STDR_1 a) = pullActivation a
pullActivations (DROPOUT a) = pullActivation a
pullActivations (STD_BIAS a) = pullActivation a