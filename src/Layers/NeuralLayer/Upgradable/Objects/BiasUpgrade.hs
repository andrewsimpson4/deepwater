{-# LANGUAGE DeriveGeneric #-}
module Layers.NeuralLayer.Upgradable.Objects.BiasUpgrade
    (
        STANDARD_BIASED_UPGRADE(..)
    ) where


import           ActivationFunctions                      as Act
import           DeepData.DeepData                        as D
import           General                                  as G
import           Control.DeepSeq
import           GHC.Generics                             (Generic)

import           Layers.NeuralLayer.Upgradable.UpgradeTypes
import           Layers.NeuralLayer.Upgradable.Objects.StdUpgrade
import System.Random

data STANDARD_BIASED_UPGRADE = STANDARD_BIASED_UPGRADE D D Activation deriving (Generic, Show)
instance NFData STANDARD_BIASED_UPGRADE

instance Upgradable STANDARD_BIASED_UPGRADE where
--   genUpgrade = genBIAS
  ffUpgrade = ffBIAS
  bpUpgrade = bpBIAS
  upUpgrade = upBIAS
  ffPullUpgrade = pullBIAS
  pullActivation = pullBiasActivation


-- genBIAS :: STANDARD_BIASED_UPGRADE -> [Int] -> Seed -> Maybe STANDARD_BIASED_UPGRADE
-- genBIAS (STANDARD_BIASED_UPGRADE d _ f) dim _ = Just $ STANDARD_BIASED_UPGRADE d (newDeepMatrix (take (product dim) (repeat 1)) dim) f

ffBIAS :: STANDARD_BIASED_UPGRADE -> K -> D -> STANDARD_BIASED_UPGRADE
ffBIAS (STANDARD_BIASED_UPGRADE _ b (Funcs base div s)) k d =
  STANDARD_BIASED_UPGRADE (base $ mult !+! bT) bT (Funcs base div s)
  where
    mult = k *** d
    bb = getB b
    getB :: D -> D
    getB b_ 
        | b_ == empty = newDeepMatrix (take (product $ shape mult) (repeat 1)) (shape mult)
        | otherwise = b_

    bT = newDeepMatrix (take (product $ shape mult) (repeat 1)) (shape mult)


bpBIAS :: STANDARD_BIASED_UPGRADE -> D -> D -> D -> STANDARD_BIASED_UPGRADE
bpBIAS (STANDARD_BIASED_UPGRADE _ b (Funcs base div s)) w delta d =
  STANDARD_BIASED_UPGRADE ((delta *** deepTranspose w) !*! div d) b (Funcs base div s)


upBIAS :: STANDARD_BIASED_UPGRADE -> D -> D -> D -> STANDARD_BIASED_UPGRADE
upBIAS (STANDARD_BIASED_UPGRADE _ b (Funcs base div s)) d k w =
    STANDARD_BIASED_UPGRADE (w !+! (deepTranspose k *** d)) (b) (Funcs base div s)

pullBIAS :: STANDARD_BIASED_UPGRADE -> D
pullBIAS (STANDARD_BIASED_UPGRADE d _ _) = d

pullBiasActivation :: STANDARD_BIASED_UPGRADE -> Maybe Activation
pullBiasActivation (STANDARD_BIASED_UPGRADE _ _ a) = Just a