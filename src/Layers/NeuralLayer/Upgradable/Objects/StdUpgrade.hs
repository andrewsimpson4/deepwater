{-# LANGUAGE DeriveGeneric #-}
module Layers.NeuralLayer.Upgradable.Objects.StdUpgrade
    (
        STANDARD_1(..)
    ) where

import           ActivationFunctions                      as Act
import           DeepData.DeepData                        as D
import           General                                  as G
import           Control.DeepSeq
import           GHC.Generics                             (Generic)

import           Layers.NeuralLayer.Upgradable.UpgradeTypes

data STANDARD_1 =
  STANDARD_1 D
             Activation
  deriving (Generic, Show)

instance NFData STANDARD_1

instance Upgradable STANDARD_1 where
  -- genUpgrade = genSTD
  ffUpgrade = ffSTRD
  bpUpgrade = bpSTRD
  upUpgrade = upSTRD
  ffPullUpgrade = pullSTRD
  pullActivation = pullStdActivation

-- genSTD :: STANDARD_1 -> [Int] -> Seed -> Maybe STANDARD_1
-- genSTD _ _ _ = Nothing

ffSTRD :: STANDARD_1 -> K -> D -> STANDARD_1
ffSTRD (STANDARD_1 _ (Funcs base div s)) k d =
  STANDARD_1 (base $ k *** d) (Funcs base div s)

bpSTRD :: STANDARD_1 -> D -> D -> D -> STANDARD_1
bpSTRD (STANDARD_1 _ (Funcs base div s)) w delta d =
  STANDARD_1 ((delta *** deepTranspose w) !*! div d) (Funcs base div s)

upSTRD :: STANDARD_1 -> D -> D -> D -> STANDARD_1
upSTRD (STANDARD_1 _ (Funcs base div s)) d k w =
    STANDARD_1 (w !+! (deepTranspose k *** d)) (Funcs base div s)

pullSTRD :: STANDARD_1 -> D
pullSTRD (STANDARD_1 d _) = d

pullStdActivation :: STANDARD_1 -> Maybe Activation
pullStdActivation (STANDARD_1 _ a) = Just a