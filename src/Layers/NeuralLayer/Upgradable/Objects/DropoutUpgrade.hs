{-# LANGUAGE DeriveGeneric #-}
module Layers.NeuralLayer.Upgradable.Objects.DropoutUpgrade
    (
        DROPOUT_UPGRADE(..)
    ) where


import           ActivationFunctions                      as Act
import           DeepData.DeepData                        as D
import           General                                  as G
import           Control.DeepSeq
import           GHC.Generics                             (Generic)

import           Layers.NeuralLayer.Upgradable.UpgradeTypes
import           Layers.NeuralLayer.Upgradable.Objects.StdUpgrade
import System.Random

data DROPOUT_UPGRADE = DROPOUT_UPGRADE D deriving (Generic, Show)
instance NFData DROPOUT_UPGRADE

instance Upgradable DROPOUT_UPGRADE where
--   genUpgrade = genSTD
  ffUpgrade = ffDROP
  bpUpgrade = bpDROP
  upUpgrade = upDROP
  ffPullUpgrade = pullDrop
  pullActivation = pullActivations

-- genSTD :: DROPOUT_UPGRADE -> [Int] -> Seed -> Maybe DROPOUT_UPGRADE
-- genSTD _ _ _ = Nothing

ffDROP :: DROPOUT_UPGRADE -> K -> D -> DROPOUT_UPGRADE
ffDROP _ _ d = DROPOUT_UPGRADE (bernoulliDrop d)

bpDROP :: DROPOUT_UPGRADE -> D -> D -> D -> DROPOUT_UPGRADE
bpDROP _ _ _ d = DROPOUT_UPGRADE d

upDROP :: DROPOUT_UPGRADE -> D -> D -> D -> DROPOUT_UPGRADE
upDROP = bpDROP

pullDrop :: DROPOUT_UPGRADE -> D
pullDrop (DROPOUT_UPGRADE d) = d

pullActivations :: DROPOUT_UPGRADE -> Maybe Activation
pullActivations _ = Nothing

bernoulli :: Int -> Int -> Int
bernoulli i p = bin (fst $ randomR (1,100) (mkStdGen i) :: Int)
    where
        bin :: Int -> Int
        bin a 
            | a >= p = 1
            | otherwise = 0

bernoulliDrop :: D -> D
bernoulliDrop (D a b) = D (run a (bernoulli 34252 30) 0) b
    where
        size = last b
        run :: [Double] -> Int -> Int -> [Double]
        run [] _ _ = []
        run (x : xs) bernoul i
            | i `mod` size == 0 = x * fromIntegral bernoul : run xs (bernoulli (34252 * i) 30) (i + 1)
            | otherwise = x * fromIntegral bernoul : run xs bernoul (i + 1)