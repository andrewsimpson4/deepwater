{-# LANGUAGE DeriveGeneric #-}

module Layers.NeuralLayer.NeuralLayer
  ( NeuralLayer(..)
    ,blankUpgrade
    ,makeStrdUpgrades
    ,makeDropoutUpgrades
    , makeStdBiasUpgrades
    ,(<-||->) 
  ) where

import           ActivationFunctions                       as Act
import           DeepData.DeepData                         as D
import           General                                   as G
import           Layers.LayerClass                         as LC
import           Layers.NeuralLayer.NeuralLayerTypes
import           Layers.NeuralLayer.Upgradable.Upgrades
import           Layers.NeuralLayer.Upgradable.UpgradeTypes
-- import           Layers.NeuralLayer.Upgrades.UpgradeTypes
import           Metrics.Metrics

import           Control.DeepSeq
import           GHC.Generics                              (Generic)
import Layers.NeuralLayer.Upgradable.GenUpgrades
-- import           Layers.NeuralLayer.Upgrades.ValidUpgrades


instance Layer NeuralLayer where
  genWt = genWt_
  feedForward = feedForward_
  backprop = backprop_
  pull = pull_
  func = activation_
  update = update_
  ends = ends_

genWt_ :: NeuralLayer -> Int -> Seed -> NeuralLayer
genWt_ (NeuralLayer i _ ups) dim sd =
  NeuralLayer i (newDeepRandomMatrix [dim, i] sd) ups
  -- where
  --   genUps :: Upgrades -> Upgrades
  --   genUps (Upgrades ups) = Upgrades [(\x -> case (x) of
  --                             Just a -> a
  --                             Nothing -> u) (genUpgrade u [dim, i] sd) | u <- ups]

feedForward_ :: NeuralLayer -> K -> NeuralLayer
feedForward_ = aplyFFUps
  -- NeuralLayer i (base (k *** d)) (Funcs base div s) ups

backprop_ :: NeuralLayer -> D -> D -> NeuralLayer
backprop_ = aplyBPUps
  -- NeuralLayer i ((delta *** deepTranspose w) !*! div k) (Funcs base div s) ups

-- backprop_ (NeuralLayer i k (Funcs base div s) ups) w delta =
update_ :: NeuralLayer -> D -> D -> NeuralLayer
update_ = aplyUPUps
-- update_ (NeuralLayer i w ups) d k =
--   NeuralLayer i (w !+! (deepTranspose k *** d)) ups

pull_ :: NeuralLayer -> D
pull_ (NeuralLayer _ d _) = d

ends_ :: NeuralLayer -> BaseLayer -> NeuralLayer
ends_ _ (BaseLayer a b c) = NeuralLayer a b blankUpgrade

activation_ :: NeuralLayer -> Activation
activation_ (NeuralLayer _ _ (Upgrades ups)) = searchActivation ups

searchActivation :: [ValidUpgrade] -> Activation
searchActivation (u : ups) = case (pullActivation u) of
                                        Just act -> act
                                        Nothing  -> searchActivation ups

-- apply feed forward updates
aplyFFUps :: NeuralLayer -> K -> NeuralLayer
aplyFFUps (NeuralLayer i d (Upgrades ups)) k =
  NeuralLayer
    i
    (ffPullUpgrade $ last mapedUpgrades)
    (Upgrades mapedUpgrades)
  where
    mapedUpgrades = reverse $ mapUpgrades ups d
    mapUpgrades :: [ValidUpgrade] -> D -> [ValidUpgrade]
    mapUpgrades [] _ = []
    mapUpgrades (u:ups) d_ = up : mapUpgrades ups (ffPullUpgrade up)
      where
        up = ffUpgrade u k d_

aplyBPUps :: NeuralLayer -> D -> D -> NeuralLayer
aplyBPUps (NeuralLayer i k (Upgrades ups)) w delta =
  NeuralLayer
    i
    (ffPullUpgrade $ last mapedUpgrades)
    (Upgrades mapedUpgrades)
  where
    mapedUpgrades = reverse $ mapUpgrades ups k
    mapUpgrades :: [ValidUpgrade] -> D -> [ValidUpgrade]
    mapUpgrades [] _ = []
    mapUpgrades (u:ups) d_ = up : mapUpgrades ups (ffPullUpgrade up)
      where
        up = bpUpgrade u w delta d_

aplyUPUps :: NeuralLayer -> D -> D -> NeuralLayer
aplyUPUps (NeuralLayer i w (Upgrades ups)) d k =
  NeuralLayer
    i
    (ffPullUpgrade $ last mapedUpgrades)
    (Upgrades mapedUpgrades)
  where
    mapedUpgrades = reverse $ mapUpgrades ups w
    mapUpgrades :: [ValidUpgrade] -> D -> [ValidUpgrade]
    mapUpgrades [] _ = []
    mapUpgrades (u:ups) d_ = up : mapUpgrades ups (ffPullUpgrade up)
      where
        up = upUpgrade u d k d_
