{-# LANGUAGE ConstrainedClassMethods #-}
{-# LANGUAGE DeriveGeneric           #-}

module Layers.NeuralLayer.NeuralLayerTypes
  ( NeuralLayer(..),
    Upgrades(..)
  ) where

import           ActivationFunctions                       as Act
import           Control.DeepSeq
import           DeepData.DeepData                         as D
import           General                                   as G
import           GHC.Generics                              (Generic)
import           Layers.NeuralLayer.Upgradable.Upgrades

data NeuralLayer =
  NeuralLayer Int
              D
              Upgrades
  deriving (Generic, Show)

instance NFData NeuralLayer


-- data Upgrades = Upgrades
--   { feedForwardUpgrades :: [ValidUpgrade]
--   , backpropUpgrades    :: [ValidUpgrade]
--   } deriving (Generic, Show)

data Upgrades = Upgrades [ValidUpgrade] deriving (Generic, Show)

instance NFData Upgrades
