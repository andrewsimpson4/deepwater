{-# LANGUAGE MultiParamTypeClasses #-}

module Layers.LayerClass
    ( 
        Layer(..),
        LayerGPU(..),
        BaseLayer(..)
       -- LayerCore(..)
    ) where
import General as G
import DeepData.DeepData as D
import ActivationFunctions as Act

data BaseLayer = BaseLayer Int D Activation deriving(Show)

class Layer a where
    genWt :: a -> Int -> Seed -> a
    feedForward :: a -> K -> a
    backprop :: a -> D -> D -> a
    update :: a -> D -> D -> a
    pull :: a -> D
    func :: a -> Activation
    ends :: a -> BaseLayer -> a
    
class LayerGPU a where
    feedForwardGPU :: a -> K -> IO a
    backpropGPU :: a -> D -> D -> IO a
    updateGPU :: a -> a -> D -> IO a
-- class LayerCore a b where
--     feedForward :: a -> K -> b
--     backprop :: b -> a -> D -> a