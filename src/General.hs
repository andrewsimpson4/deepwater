
module General
( 
  X,
  Y,
  K,
  Seed,
  Steps,
  Size,
  BatchSize
) where

import DeepData.DeepData as D

type X = D
type Y = D
type K = D
type Seed = Int
type Steps = Int
type Size = Int
type BatchSize = Int