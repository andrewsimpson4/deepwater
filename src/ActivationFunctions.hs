{-# LANGUAGE DeriveGeneric #-}

module ActivationFunctions
( 
  Base(..),
  Derivation(..),
  Activation(..),
  sigmoid,
  sigmoid_,
  sigmoidDeriv_,
  relu,
  linear,
  binary,
  tanhh,
  leakyRelu
) where

import GHC.Generics (Generic)
import Control.DeepSeq
import DeepData.DeepData

type Base = (D -> D)
type Derivation = (D -> D)
data Activation = Funcs Base Derivation String deriving(Generic)
instance NFData Activation

instance Show Activation where
    show (Funcs _ _ s) = show s


sigmoid :: Activation
sigmoid = Funcs sigmoid_ sigmoidDeriv_ "sigmoid"

sigmoid_ :: D -> D
sigmoid_ = applyFunction (\ x -> 1 / (1 + exp (-x)))

sigmoidDeriv_ :: D -> D
sigmoidDeriv_ = applyFunction (\x -> x*(1-x))




relu :: Activation
relu = Funcs relu_ reluDerive_ "relu"

relu_ :: D -> D
relu_ = applyFunction (\x -> case () of
                                    _ | x > 0 -> x
                                      | otherwise -> 0)
reluDerive_ :: D -> D
reluDerive_ = applyFunction (\x -> case () of
                                    _ | x > 0 -> 1
                                      | otherwise -> 0)




linear :: Activation
linear = Funcs linear_ linearDerive_ "linear"

linear_ :: D -> D
linear_ = applyFunction id

linearDerive_ :: D -> D
linearDerive_ = applyFunction (const 1)




binary :: Activation
binary = Funcs binary_ binaryDerive_ "binary"

binary_ :: D -> D
binary_ = applyFunction (\x -> case () of
                                    _ | x >= 0 -> 1
                                      | otherwise -> 0)

binaryDerive_ :: D -> D
binaryDerive_ = applyFunction (const 0)




tanhh :: Activation
tanhh = Funcs tanh_ tanhDerive_ "tanh"

tanh_ :: D -> D
tanh_ = applyFunction (\x -> 2 / (1 + exp(-2 * x)) - 1)

tanhDerive_ :: D -> D
tanhDerive_ xx = applyFunction (\x -> 1 - x ** 2) (tanh_ xx)




leakyRelu :: Activation
leakyRelu = Funcs leakyRelu_ leakyReluDerive_ "leakyRelu"

leakyRelu_ :: D -> D
leakyRelu_ = applyFunction (\x -> case () of
                                    _ | x >= 0 -> x
                                      | otherwise -> 0.01 * x)

leakyReluDerive_ :: D -> D
leakyReluDerive_ = applyFunction (\x -> case () of
                                            _ | x >= 0 -> 1
                                              | otherwise -> 0.01)



-- softmax_ :: D -> D
-- softmax_ d = applyFunction (\x -> exp x / sumd) d
--     where
--         sumd = sum $ matrix d



