
module BaseGPU
    ( 
        deepLearnerGPU
    ) where

import DeepData.DeepData as D
import General as G
import Layers.FullyConnectedLayer as FC
import ActivationFunctions as Act
import Layers.LayerClass as LC
import Layers.DropoutLayer as DP
import BaseTypes as BL


deepLearnerGPU :: X -> Y -> [Layers] -> Steps -> Seed -> IO Weights
deepLearnerGPU x y c steps seed = epoch iwts steps
    where
        iwts :: Weights
        iwts = wtsInit c (last $ shape x) seed
        epoch :: Weights -> Int -> IO Weights
        epoch wts i
            | i == 0 = nw
            | otherwise = do
                n <- nw
                epoch n (i - 1)
            where
                nw :: IO Weights 
                nw = do 
                        d <- dss
                        g <- gs
                        reverse <$> upd (reverse wts) d g

                aks :: IO Alphas
                aks = ks wts x
                
                dss :: IO Deltas
                dss = do 
                        k <- aks
                        ds (reverse k) (reverse $ (\(_ : ws) -> ws) wts) y

                gs :: IO Gammas
                gs = do 
                        k <- aks
                        return $ gammas (reverse k) x

                upd :: Weights -> Deltas -> Gammas -> IO Weights
                upd [] [] [] = return []
                upd (w : ws) (d : ds_) (k : ks_) = do 
                                                        n <- updateGPU w d (pull k) 
                                                        nxt <- upd ws ds_ ks_
                                                        return $ n : nxt
    
ks :: Weights -> K -> IO Alphas
ks [] _ = return []
ks (w :ws) k = do 
    n <- feedForwardGPU w k
    nxt <- ks ws (pull n)
    return $ n : nxt

ds :: Alphas -> Weights -> Y -> IO Deltas
ds (kI : ksI) w y = do
                        let ini = ends kI (BaseLayer (last $ shape init) init sigmoid)
                        dn <- dsRec ksI w init
                        return $ ini : dn
    where
        init :: D
        init = backInit y (pull kI) (func kI)

        dsRec :: Alphas -> Weights -> D -> IO Deltas
        dsRec [] [] _ = return []
        dsRec (k : ks) (w : ws) delta = do 
                                            n <- backpropGPU k (pull w) delta 
                                            nxt <- dsRec ks ws (pull n)
                                            return $ n : nxt



wtsInit :: [Layers] -> Int -> Seed -> Weights
wtsInit [] _ _ = []
wtsInit (l : ls) dim sd = nxt : wtsInit ls (last $ shape $ pull nxt) (sd + 1)
    where
        nxt = genWt l dim sd

gammas :: Weights -> D -> Gammas
gammas a x = reverse $ (\(t : as) -> ends t (BaseLayer (last $ shape x) x sigmoid) : reverse as) a

backInit :: Y -> K -> Activation -> D
backInit  y k (Funcs act derivative _) = (y !-! k) !*! derivative k

predictGPU :: X -> Weights -> IO Weights
predictGPU x s = take 1 . reverse <$> ks s x