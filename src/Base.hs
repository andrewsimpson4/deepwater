-- Darkside light, monokai pro(filter)
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE BangPatterns #-}
module Base
    (
        deepLearner,
        predict,
        BL.Layers(..),
        BL.fullyConnected,
        BL.dropout,
        BL.neuralLayer,
        pullBatch
    ) where

import DeepData.DeepData as D
import General as G
import Layers.FullyConnectedLayer as FC
import ActivationFunctions as Act
import Layers.LayerClass as LC
import Layers.DropoutLayer as DP
import BaseTypes as BL
import Control.DeepSeq
import LossFunctions

deepLearner
  :: X -> Y -> [Layers] -> Steps -> BatchSize -> LossFunction -> Seed -> Weights
deepLearner x y c steps batchSize loss seed = epoch iwts steps --layers (last $ shape x)
 where
  iwts :: Weights
  iwts = wtsInit c (last $ shape x) seed
  epoch :: Weights -> Int -> Weights
  epoch !w !is | is == 0   = w `deepseq` batch w is
               | -- reverse aks --reverse $ (\(_ : ws) -> ws) wts
                 otherwise = epoch (batch w 0) (is - 1)
      -- reverse $ update (reverse wts) ds gs
   where
    batch :: Weights -> Int -> Weights
    batch !wts !i
      | i
        < ceiling
            ((fromIntegral $ product ((\(_ : xs) -> xs) (reverse $ shape x)) :: Double
             )
            / (fromIntegral batchSize :: Double)
            )
        - 1
      = nw `deepseq` batch nw (i + 1)
      | otherwise
      = nw
     where
      xs, ys :: X
      !xs = case () of
        _
          | (length (matrix y) `mod` batchSize) == 0
          -> pullBatch x batchSize i
          | i < ceiling
            ((fromIntegral $ product ((\(_ : xs) -> xs) (reverse $ shape x)) :: Double
             )
            / (fromIntegral batchSize :: Double)
            )
          -> pullBatch x batchSize i
          | otherwise
          -> pullBatch x (length (matrix x) `mod` batchSize) i
      !ys = case () of
        _
          | (length (matrix y) `mod` batchSize) == 0
          -> pullBatch y batchSize i
          | i < ceiling
            ((fromIntegral $ product ((\(_ : xs) -> xs) (reverse $ shape x)) :: Double
             )
            / (fromIntegral batchSize :: Double)
            )
          -> pullBatch y batchSize i
          | otherwise
          -> pullBatch x (length (matrix y) `mod` batchSize) i

      nw :: Weights
      !nw = reverse $ upd (reverse wts) dss gs

      aks :: Alphas
      !aks = ks wts xs

      dss :: Deltas
      !dss = ds (reverse aks) (reverse $ (\(_ : ws) -> ws) wts) ys loss

      gs :: Gammas
      !gs = gammas (reverse aks) xs

      upd :: Weights -> Deltas -> Gammas -> Weights
      upd []       []        []        = []
      upd (w : ws) (d : ds_) (k : ks_) = update w (pull d) (pull k) : upd ws ds_ ks_

ks :: Weights -> K -> Alphas
ks []       _ = []
ks (w : ws) k = nk : ks ws (pull nk)
 where
  nk :: Layers
  nk = feedForward w k

--init : back  ksx (reverse $ (\(_ : ws) -> ws) wts) init
ds :: Alphas -> Weights -> Y -> LossFunction -> Deltas
ds (kI : ksI) w y loss =
  ends kI (BaseLayer (last $ shape init) init sigmoid) : dsRec ksI w init
 where
  init :: D
  init = backInit y (pull kI) (func kI) loss

  dsRec :: Alphas -> Weights -> D -> Deltas
  dsRec []       []       _     = []
  dsRec (k : ks) (w : ws) delta = nd : dsRec ks ws (pull nd)
   where
    nd :: Layers
    nd = backprop k (pull w) delta


wtsInit :: [Layers] -> Int -> Seed -> Weights
wtsInit []       _   _  = []
wtsInit (l : ls) dim sd = nxt : wtsInit ls (last $ shape $ pull nxt) (sd + 1)
  where nxt = genWt l dim sd

gammas :: Weights -> D -> Gammas
gammas a x =
  reverse
    $ (\(t : as) -> ends t (BaseLayer (last $ shape x) x sigmoid) : reverse as)
        a

backInit :: Y -> K -> Activation -> LossFunction -> D
backInit y k (Funcs act derivative _) (LossFunction loss) =
  loss y k !*! derivative k

predict :: X -> Weights -> D
predict x s = pull $ last (ks s x)

pullBatch :: X -> Int -> Int -> X
pullBatch x i from
  | i == 0    = D [] []
  | -- indexPull from x
    otherwise = pullBatch x (i - 1) from <-> indexPull (i + from - 1) x