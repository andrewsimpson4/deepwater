{-# LANGUAGE ParallelListComp #-}
module Metrics.Metrics
    ( 
        meanAbsoluteError
    ) where

import DeepData.DeepData as D

meanAbsoluteError :: D -> D -> Double
meanAbsoluteError (D a _) (D b _) = sum [abs (x - y) | x <- a | y <- b] / fromIntegral (length a) :: Double
