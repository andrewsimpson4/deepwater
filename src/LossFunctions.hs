
module LossFunctions
(
LossFunction(..),
differnce,
crossEntropyLoss
) where

import DeepData.DeepData as D

newtype LossFunction = LossFunction (D -> D -> D)

differnce, crossEntropyLoss :: LossFunction
differnce = LossFunction differnce_
crossEntropyLoss = LossFunction crossEntropyLoss_

differnce_ :: D -> D -> D
differnce_ y y1 = y !-! y1

crossEntropyLoss_ :: D -> D -> D
crossEntropyLoss_ = mixBy crossEntropy

crossEntropy :: Double -> Double -> Double
crossEntropy y p = -( (y*logBase 10 p) + ((1 - y)*logBase 10 (1-p)) )

-- meanSquareError :: D -> D -> D
-- meanSquareError = mixBy meanSquare

-- meanSquare :: Double -> Double -> Double


