{-# LANGUAGE DeriveGeneric #-}
module DeepData.DeepDataTypes
( 
    D(..)
) where
import GHC.Generics (Generic)
import Control.DeepSeq

data D = D [Double] [Int] deriving (Generic, Show, Read, Eq)
instance NFData D