#pragma OPENCL EXTENSION cl_amd_printf : enable

__kernel void vectorAdd(__global const double *a, __global const double *b, __global double *c) {
    int i = get_global_id(0);
    c[i] = 0;
   // c[i] = (double)((int)a[i] + (int)b[i]);
    for (int count = 0; count < 4000; count++) {
        c[i] = c[i] + (double)((int)a[i] + (int)b[i]);
    }
}

__kernel void preper(__global const float *a, __global float *c) {
    int i = get_global_id(0);
    int gamma = i;
    // c[i] = a[2];
   
    int lamda = (int)a[0];
    int beta = (int)a[1];
    int psi = (int)a[2];
    int alpha = (int)a[3];
    int mu = (int)a[4];

    c[i] = (lamda * (gamma % beta)) + ((gamma / beta) % lamda) + (mu * (gamma * alpha / psi));
    // c[i] = (lamda * (gamma % beta)) + (gamma * lamda / psi) + (mu * (gamma * alpha / psi));
}

__kernel void mmul(__global const float *p, __global const float *a,
                   __global const float *b, __global float *c) {

    int i = get_global_id(0);
    // c[i] = i + 1;
    int size = (int)p[0];
    int max2 = (int)p[1];


    // for(int x = 0; x < max2; x++) {
    //     c[(i * max2) + x] = 0;
    //     for (int y = 0; y < size; y++) {
    //          c[(i * max2) + x] =  c[(i * max2) + x] + (a[(i * size) + y] * b[(x * size) + y]);
    //     }
    // }
    

    int ia = (i / max2) * size;
    int ib = (i % max2) * size;
    
    c[i] = 0;
    for (int z = 0; z < size; z++) {
        c[i] = c[i] + (a[ia + z] * b[ib + z]);
    }
    


}