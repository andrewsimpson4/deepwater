{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE BangPatterns #-}
module DeepData.Gpu.DeepGpu
    ( 
        mainGPU,
        -- mainGPUInfo,
        mmGpu,
        (<***>),
        runSum,
        scaleBetween
    ) where

import Data.List (intercalate)

import Control.Monad
import Control.Applicative

import Foreign.OpenCL.Bindings
import Foreign.Storable

import qualified Data.ByteString as B

import System.Mem
import Control.Concurrent

import qualified Data.Vector as V

import qualified DeepData.DeepDataTypes as D
import Control.DeepSeq

-- platformInfoFn = [
--     platformName,
--     platformVendor,
--     platformVersion,
--     platformProfile,
--     \x -> intercalate ", " <$> platformExtensions x]

-- mainGPUInfo :: IO ()
-- mainGPUInfo = do
--     ps <- getPlatformIDs
--     putStrLn $ (show $ length ps) ++ " platform(s) found:"
--     sequence_ [info p >>= putStrLn . ("    " ++) |
--                 p <- ps,
--                 info <- platformInfoFn]
--     putStrLn ""
--     devices <- liftM concat $ mapM (getDeviceIDs [DeviceTypeAll]) ps
--     putStrLn $ (show $ length devices) ++ " devices(s) found:"
--     forM_ devices $ \dev -> do
--     putStrLn ""
--     mapM (printDevice dev) devinfo

-- -- Device info functions
-- data DevInfo = forall a. Show a => D String (DeviceID -> IO a)

-- printDevice device (D name deviceInfoFn) = do
--     info <- deviceInfoFn device
--     putStrLn $ "    Device " ++ name ++ ": " ++ show info

-- devinfo = [
--     D "name" deviceName,
--     D "type" deviceType,
--     D "vendor" deviceVendor,
--     D "vendor id" deviceVendorID,
--     D "version" deviceVersion,
--     D "driver version" deviceDriverVersion,
--     D "profile" deviceProfile,
--     D "available" deviceAvailable,
--     D "compiler available" deviceCompilerAvailable,  
--     D "execution capabilities" deviceExecutionCapabilities,
--     D "queue properties" deviceQueueProperties,
    
--     D "max clock frequency" deviceMaxClockFrequency,
--     D "max compute units" deviceMaxComputeUnits,
--     D "max constant args" deviceMaxConstantArgs,
--     D "max parameter size" deviceMaxParameterSize,
--     D "max work group size" deviceMaxWorkGroupSize,
--     D "max work item dimensions" deviceMaxWorkItemDimensions,
--     D "max work item sizes" deviceMaxWorkItemSizes,
    
--     D "global mem cache type" deviceGlobalMemCacheType,
--     D "global mem cache size" deviceGlobalMemCacheSize,
--     D "global mem cache line size" deviceGlobalMemCacheLineSize,
--     D "global mem size" deviceGlobalMemSize,
--     D "local mem size" deviceLocalMemSize,
--     D "local mem type" deviceLocalMemType,
--     D "max mem alloc size" deviceMaxMemAllocSize,
--     D "max constant buffer size" deviceMaxConstantBufferSize,

--     D "address bits" deviceAddressBits,
--     D "little endian" deviceEndianLittle,
--     D "mem base addr align" deviceMemBaseAddrAlign,
--     D "min data type align size" deviceMinDataTypeAlignSize,
--     D "preferred vector width char" devicePreferredVectorWidthChar,
--     D "preferred vector width short" devicePreferredVectorWidthShort,
--     D "preferred vector width int" devicePreferredVectorWidthInt,  
--     D "preferred vector width long" devicePreferredVectorWidthLong,
--     D "preferred vector width float" devicePreferredVectorWidthFloat,
--     D "error correction support" deviceErrorCorrectionSupport,
--     D "profiling timer resolution" deviceProfilingTimerResolution,
    
--     D "image support" deviceImageSupport,
--     D "max read image args" deviceMaxReadImageArgs,
--     D "max write image args" deviceMaxWriteImageArgs,
--     D "max samplers" deviceMaxSamplers,
--     D "image 2d max size" deviceImage2DMaxSize,
--     D "image 3d max size" deviceImage3DMaxSize,

--     D "extensions" deviceExtensions
--     ]


n :: Int
n = 15000000

list0, list1 :: [Float]
list0 = [1..fromIntegral n]
list1 = [1..fromIntegral n]

runSum :: IO ()
runSum = do
    let x = summ list0 list1
    print $ x `deepseq` last $ x
summ :: [Float] -> [Float] -> [Float]
summ [] [] = []
summ (a : as) (b : bs) = loop 2000 : summ as bs
    where
        loop :: Int -> Float
        loop 0 = 0
        loop i = a + b + loop (i - 1)
        

load :: IO ([Float], [Float])
load = do
    let x = list0
    let y = list1
    x `deepseq` y `deepseq` return (x,y)

mainGPU :: IO ()
mainGPU = do
  platform <- head <$> getPlatformIDs
  devices <- getDeviceIDs [DeviceTypeAll] platform
  let device = devices !! 1
  context <- createContext [device] [ContextPlatform platform] NoContextCallback
  queue <- createCommandQueue context device [QueueProfilingEnable]
  
  prog <- createProgram context =<< readFile "./src/DeepData/Gpu/vectorCL.cl"
  buildProgram prog [device] ""
  kernel <- createKernel prog "vectorAdd"
  
  array0 <- newListArray context list0
  array1 <- newListArray context list1
  out <- mallocArray context [MemWriteOnly] n :: IO (MemObject Float)

  setKernelArgs kernel [MObjArg array0, MObjArg array1, MObjArg out]
  event <- enqueueNDRangeKernel queue kernel [] [fromIntegral n] [] []
  
  list <- peekListArray queue n out
  let !ll = list
  -- print ll
  --free array0
  --free array1
  --free out
  --performGC
  print $ list `deepseq` last ll


infixr 6 <***>

(<***>) :: D.D -> D.D -> IO D.D
a <***> b = do
    m <- mmGpu a b
    return $ D.D m (dim a b) 

mmGpu :: D.D -> D.D -> IO [Double]
mmGpu (D.D a1 b1) (D.D a2 b2) =
    if verifyMul (D.D a1 b1) (D.D a2 b2) then do
        platform <- head <$> getPlatformIDs
        devices <- getDeviceIDs [DeviceTypeAll] platform
        let device = devices !! 1
        context <- createContext [device] [ContextPlatform platform] NoContextCallback
        queue <- createCommandQueue context device [QueueProfilingEnable]

        prog <- createProgram context =<< readFile "./src/DeepData/Gpu/vectorCL.cl"
        buildProgram prog [device] ""
        kernel <- createKernel prog "preper"

        let 
            λ, β, ψ, α, μ :: Int
            λ = b2 !! (length b2 - 1)
            β = b2 !! (length b2 - 2)
            ψ = length a2
            μ = λ * β
            α = quot ψ μ
        -- indexes <- newListArray context ([0,1,2,3,4,5,6,7,8,9,10,11] :: [Float])
        params <- newListArray context [fromIntegral λ :: Float, fromIntegral β :: Float, fromIntegral ψ :: Float, fromIntegral α :: Float, fromIntegral μ :: Float]
        out <- mallocArray context [MemWriteOnly] ψ :: IO (MemObject Float)

        setKernelArgs kernel [MObjArg params, MObjArg out]
        event <- enqueueNDRangeKernel queue kernel [] [fromIntegral ψ] [] []
        list <- peekListArray queue ψ out
        let !preped = list
            !aV = V.fromList a2 :: V.Vector Double
            !sndV = prepMove aV preped
        free params
        free out
        -- sndV `deepseq` return sndV
        -- performGC
        -- print "Preps"
        -- print sndV
        sndV `deepseq` mul (D.D a1 b1) sndV context queue prog
    else error ("ERROR MUL -> uneven sizes <-" ++ show b1 ++ "<->" ++ show b2)
mul :: D.D -> [Double] -> Context -> CommandQueue -> Program -> IO [Double]
mul (D.D a1 b1) a2 context queue program = run
    where
        size = last b1
        max1 = quot (length a1) size
        max2 = quot (length a2) size
        outSize = max1 * max2
        cycles = max1 * max2
        run :: IO [Double]
        run = do
            kernel <- createKernel program "mmul"

            params <- newListArray context [fromIntegral size :: Float, fromIntegral max2 :: Float]
            as <- newListArray context (map (\x -> realToFrac x :: Float) a1)
            bs <- newListArray context (map (\x -> realToFrac x :: Float) a2)
            out <- mallocArray context [MemWriteOnly] outSize :: IO (MemObject Float)
            
            setKernelArgs kernel [MObjArg params, MObjArg as, MObjArg bs, MObjArg out]
            event <- enqueueNDRangeKernel queue kernel [] [fromIntegral cycles] [] []
            list <- peekListArray queue outSize out
            let !new = list
            free params
            free as
            free bs
            free out
            -- performGC
            return $ map (\x -> realToFrac x :: Double) new
                

prepMove :: V.Vector Double -> [Float] -> [Double]
prepMove d = run
    where
        run :: [Float] -> [Double]
        run = map (\ x -> d V.! floor x)

verifyMul :: D.D -> D.D -> Bool
verifyMul (D.D _ a) (D.D _ b)
    | last a == b !! (length b - 2) = True
    | otherwise = False


dim :: D.D -> D.D -> [Int]
dim (D.D a d1) (D.D b d2) = take (m_1 $ length d1) d1 ++ take (m_1 $ m_1 $ length d2) d2 ++ take 1 (reverse d2)
    where 
        m_1 :: Int -> Int
        m_1 m = m - 1

scaleBetween :: Double -> Double -> [Double] -> [Double]
scaleBetween l u d = map (\ x -> (x * (u - l) / max) + l ) d
    where
        max = maximum d

        
-- gpuLoop :: Int -> IO Float
-- gpuLoop i 
--     | i == 0 = mainGPU
--     | otherwise = do
--         let !x = mainGPU
--         gpuLoop (i - 1)




