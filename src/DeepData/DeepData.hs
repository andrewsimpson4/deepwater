-- UI Hawaii ... Darkside ... Henna ... frontier

{-# LANGUAGE GADTs #-}
{-# LANGUAGE ParallelListComp #-}
{-# LANGUAGE UnicodeSyntax #-}
{-# LANGUAGE BangPatterns #-}
module DeepData.DeepData
    (
        D(D),
        newDeepMatrix,
        (***),
        (!-!),
        (!+!),
        (!*!),
        (!/!),
        (>>),
        (|->),
        (<->),
        newDeepRandomMatrix,
        mixBy,
        shape,
        matrix,
        takeSampleTo,
        applyFunction,
        --inverse,
        takeSampleLast,
        test,
        WT (..),
        deepTranspose,
        mMulVector,
        toVector,
        dToGroups,
        gpuPreper,
        tanhNorm,
        sigNorm,
        indexPull,
        unSigNorm,
        empty
    ) where

import System.Random
import qualified Data.Vector as V
import DeepData.Gpu.DeepGpu
import DeepData.DeepDataTypes
-- import qualified Data.Sequence as S
-- import qualified Prelude as P

infixr 6 ***
infixr 5 !-!
infixr 5 !+!
infixr 5 !*!
infixr 5 !/!
infixr 5 -->
infixr 5 |->
infixr 5 <->
-- infixr 5 !*<
-- infixr 6 <--|

-- Deep data combo
-- data Ddc a = Ddc [a] [Int] deriving (Show)
-- data DeepData a where
--     D :: (Num a, Show a) => [a] -> [Int] -> DeepData b
-- data D = D [Double] [Int] deriving (Show)
data DeDa = DeDa {-# UNPACK #-} !(V.Vector Double) {-# UNPACK #-} !(V.Vector Int)
data F = F [Int] Int [Int] deriving Show
newtype WT = WT [Double] deriving (Show)
-- data DeepData a where
--     DD :: (Num a, Fractional a, Show a) => DA a -> DeepData (DA a)
empty :: D
empty = D [] []

newDeepMatrix :: [Double] -> [Int] -> D
newDeepMatrix a b 
                | fromIntegral (length a)  == product b = D a b
                | otherwise = error "ERROR -> Dimentions given do not fit <-"
                
randomList :: Int -> [Double]
randomList seed = randomRs (-1,1) (mkStdGen seed) :: [Double]
pureSeed = randomIO :: IO Int --7532251092818877377

newDeepRandomMatrix :: [Int] -> Int -> D
newDeepRandomMatrix d s = D (take (product d) $ randomList s) d

toVector :: D -> DeDa
toVector (D a b) = DeDa (V.fromList a :: V.Vector Double) (V.fromList b :: V.Vector Int)



-- (***) :: D -> D -> D
-- a *** b = validate $ D (reverse $ mul a b 0 0 0 []) (dim a b) --DeepData (dim a b) (mul a b)
--         where 
--             mul :: D -> D -> Int -> Int -> Int -> [Double] -> [Double]
--             mul  (D a d1) (D b d2) x y k state
--                 | d1 !! m_1 (length d1) /= d2 !! m_1 (m_1 (length d2)) = error "ERROR -> invalid sizes <-"
--                 | k <= kLim = mul fa fb x y (a_1 k) (summation 0 0 : state)
--                 | y < yLim = mul fa fb x (a_1 y) 0 state
--                 | x < xLim = mul fa fb (a_1 x) 0 0 state
--                 | otherwise = state
--                 where 
--                     fa,fb :: D
--                     fa = D a d1
--                     fb = D b d2
                    
--                     kLim, yLim, xLim :: Int
--                     kLim = m_1 (d2!!(length d2-1))
--                     yLim = m_1 (product (take (m_1 $ m_1 $ length d2) d2))
--                     xLim = m_1 (product (take (m_1 $ length d1) d1))
                    
--                     summation :: Int -> Double -> Double
--                     summation j s
--                         | j <= m_1 (d1 !! m_1 (length d1)) = 
--                             summation (a_1 j) 
--                                       (s + ((a !! ((x * (d1 !! m_1 (length d1))) + j)) * 
--                                             (b !! ((j * (d2 !! m_1 (length d2))) + 
--                                             (y * (d2 !! m_1 (m_1 (length d2))) *           
--                                             (d2 !! m_1 (length d2)))+k))))
--                         | otherwise = s
                      
--             m_1 :: Int -> Int
--             m_1 m = m - 1
--             a_1 :: Int -> Int
--             a_1 m = m + 1
--             dim :: D -> D -> [Int]
--             dim (D a d1) (D b d2) = take (m_1 $ length d1) d1 ++ take (m_1 $ m_1 $ length d2) d2 ++ take 1 (reverse d2)
--             validate :: D -> D
--             validate (D a d1)
--                 | length a == product d1 = D a d1
--                 | otherwise = error "ERROR -> invalid sizes check <-"

(!-!) :: D -> D -> D
a !-! b = mixBy (-) a b

(!+!) :: D -> D -> D
a !+! b = mixBy (+) a b

(!*!) :: D -> D -> D
a !*! b = mixBy (*) a b

(!/!) :: D -> D -> D
a !/! b = mixBy (/) a b

(|->) :: Double -> D -> D
a |-> (D a1 b) = D [x * a | x <- a1] b


mixBy :: (Double -> Double -> Double) -> D -> D -> D 
mixBy f (D a d1) (D b d2)
        | d1 == d2 = D [f x y | x <- a | y <- b] d1
        | otherwise = error ("ERROR mix -> uneven sizes <-" ++ show d1 ++ "<->" ++ show d2)

shape :: D -> [Int]
shape (D _ d) = d

matrix :: D -> [Double]
matrix (D a _) = a

takeSampleTo :: Int -> D -> [Double]
takeSampleTo i (D a _) = take i a

takeSampleLast :: D -> Double
takeSampleLast (D a _) = last a

applyFunction :: (Double -> Double) -> D -> D
applyFunction f d = D [f x | x <- matrix d] (shape d)

dToGroups :: D -> ([D], [Int])
dToGroups (D a b) = (run a 0 [], b)
    where
        size = last b
        run :: [Double] -> Int -> [Double] -> [D]
        run [] _ h = []
        run (x : xs) i h
            | (i + 1) `mod` size == 0 = D (reverse (x : h)) [1,size] : run xs (i + 1) []
            | otherwise = run xs (i + 1) (x : h)
        
tanhNorm :: Double -> [Double] -> ([Double], Double)
tanhNorm t d = (map ta d, m)
    where
        m :: Double
        m = sum d / (fromIntegral $ length d :: Double)
        ta :: Double -> Double
        ta x = (2*t / (1 + exp(-(1/t)*(x-m)))) - t

sigNorm :: Double -> [Double] -> ([Double], Double)
sigNorm h d = (map ta d, m)
    where
        m :: Double
        m = sum d / (fromIntegral $ length d :: Double)
        ta :: Double -> Double
        ta x = h / (1 + exp(-(1/m)*(x - m)))

unSigNorm :: Double -> Double -> [Double] -> [Double]
unSigNorm h m = map f
    where
        f :: Double -> Double
        f x = (-m) * log ((h / x) - 1) + m
-- groupToD :: [[Double]] -> D

-- inverse :: D -> D
-- inverse (D a b) = D a (reverse b)



-- fastMulPrep :: D -> [Int]
-- fastMulPrep (D a b) = reverse (prep 0 [])
--     where 
--         λ, β, ψ, α, μ :: Int
--         λ = b !! (length b - 1)
--         β = b !! (length b - 2)
--         ψ = length a
--         α = product $ take (length b - 2) b
--         μ = λ * β
--         prep :: Int -> [Int] -> [Int]
--             | i == length a = n
--             | otherwise = prep (i + 1) (eq i : n)
        
--         eq :: Int -> Int
--         eq γ = (β * (γ `mod` λ)) + (μ * quot (γ * α) ψ) + quot γ λ `mod` β
-- eq :: Int -> Int
        -- eq γ = (β * (γ `mod` λ)) + (μ * quot (γ * α) ψ) + quot γ λ `mod` β
        --     where 
        --         λ, β, ψ, α, μ :: Int
        --         λ = tB2 !! (length tB2 - 1)
        --         β = tB2 !! (length tB2 - 2)
        --         ψ = length tA2
        --         α = product $ take (length tB2 - 2) tB2
        --         μ = λ * β

(***) :: D -> D -> D
(***) !a !b = D (mMul a b) (dim a b)

mMul :: D -> D -> [Double]
mMul (D tA1 tB1) (D tA2 tB2)
    | verifyMul (D tA1 tB1) (D tA2 tB2) = mul 0 0 0.0 --reverse (mul 0 0 0.0 []) --reverse $ (\(x : xs) -> xs) $ reverse (mul 0 0 0.0)
    | otherwise = error ("ERROR MUL -> uneven sizes <-" ++ show tB1 ++ "<->" ++ show tB2)
    where
        a, b :: DeDa
        a = DeDa (V.fromList tA1 :: V.Vector Double) (V.fromList tB1 :: V.Vector Int)
        b = DeDa (V.fromList tA2 :: V.Vector Double) (V.fromList tB2 :: V.Vector Int)
        
        pr :: [Double]
        pr = prep 0 []
        
        prep :: Int -> [Double] -> [Double]
        prep i d
            | i == length tA2 = reverse d
            | otherwise = prep (i + 1) ( (\ (DeDa xa _) -> xa) b V.! eq i : d )

        eq :: Int -> Int
        eq γ = (λ * (γ `mod` β)) + (quot γ β `mod` λ) + (μ * quot (γ * α) ψ)
             
        λ, β, ψ, α, μ :: Int
        λ = tB2 !! (length tB2 - 1)
        β = tB2 !! (length tB2 - 2)
        ψ = length tA2
        μ = λ * β
        α = quot ψ μ
        
        aa = (\(DeDa aa _) -> aa) a
        aa2 = V.fromList pr
        bb = (\(DeDa _ bb) -> bb) a
        bb2 = (\(DeDa _ bb) -> bb) b

        mul :: Int -> Int -> Double -> [Double]
        mul !i !ii !val
            | i == quot (V.product bb) (V.last bb) = []
            | ii == V.product bb2 = mul (i + 1) 0 0.0 --(val : f)
            | ii `mod` V.last bb == V.last bb - 1 = (val + ((aa V.! (ii `mod` V.last bb + (i * V.last bb))) * (aa2 V.! ii))) : mul i (ii + 1) 0.0
            | otherwise = mul i (ii + 1) (val + ((aa V.! (ii `mod` V.last bb + (i * V.last bb))) * (aa2 V.! ii)))

-- mul :: Int -> Int -> Double -> [Double] -> [Double]
        -- mul !i !ii !val !f
        --     | i == quot (V.product bb) (V.last bb) = f
        --     | ii == V.product bb2 = mul (i + 1) 0 0.0 f --(val : f)
        --     | ii `mod` V.last bb == V.last bb - 1 = mul i (ii + 1) 0.0 ((val + ((aa V.! (ii `mod` V.last bb + (i * V.last bb))) * (aa2 V.! ii))) : f)
        --     | otherwise = mul i (ii + 1) (val + ((aa V.! (ii `mod` V.last bb + (i * V.last bb))) * (aa2 V.! ii))) f


gpuPreper :: IO ()
gpuPreper = do
    let
        x1 = newDeepMatrix [0,1,2,3,4,5,6,7,8,9,10,11] [2,3,2]
        x2 = newDeepMatrix [0,1,2,3,4,5,6,7,8,9,10,11] [2,2,3] --  newDeepRandomMatrix [2,3,2] 32322
    mmGpu x1 x2
    print $ x1 *** x2
--     let 
--         λ, β, ψ, α, μ :: Int
--         λ = b !! (length b - 1)
--         β = b !! (length b - 2)
--         ψ = length a
--         α = quot ψ μ
--         μ = λ * β

--         aV :: V.Vector Double
--         aV = V.fromList a :: V.Vector Double
--     pos <- mmPrep λ β ψ α μ
--     let !i = prepMove aV pos
--     return $ last i

-- prepMove :: V.Vector Double -> [Float] -> [Double]
-- prepMove d = run
--     where
--         run :: [Float] -> [Double]
--         run = map (\ x -> d V.! floor x)



mMulVector :: DeDa -> DeDa -> V.Vector Double
mMulVector (DeDa tA1 tB1) (DeDa tA2 tB2)
    | verifyMulVector (DeDa tA1 tB1) (DeDa tA2 tB2) = mul 0 0 0.0 V.empty --reverse (mul 0 0 0.0 []) --reverse $ (\(x : xs) -> xs) $ reverse (mul 0 0 0.0)
    | otherwise = error ("ERROR MUL -> uneven sizes <-" ++ show tB1 ++ "<->" ++ show tB2)
    where
        -- a, b :: DeDa
        -- a = DeDa (V.fromList tA1 :: V.Vector Double) (V.fromList tB1 :: V.Vector Int)
        -- b = DeDa (V.fromList tA2 :: V.Vector Double) (V.fromList tB2 :: V.Vector Int)
        
        pr :: V.Vector Double
        pr = prep 0
        
        prep :: Int -> V.Vector Double
        prep !i
            | i == length tA2 = V.empty
            | otherwise = V.snoc (prep (i + 1)) (tA1 V.! eq i ) 

        eq :: Int -> Int
        eq γ = (λ * (γ `mod` β)) + (quot γ β `mod` λ) + (μ * quot (γ * α) ψ)
                
        λ, β, ψ, α, μ :: Int
        λ = tB2 V.! (V.length tB2 - 1)
        β = tB2 V.! (V.length tB2 - 2)
        ψ = V.length tA2
        μ = λ * β
        α = quot ψ μ
        
        aa = tA1 --(\(DeDa aa _) -> aa) a
        aa2 = pr
        bb = tB1
        bb2 = tB2
        mul :: Int -> Int -> Double -> V.Vector Double -> V.Vector Double
        mul !i !ii !val !f
            | i == quot (V.product bb) (V.last bb) = f
            | ii == V.product bb2 = mul (i + 1) 0 0.0 f --(val : f)
            | ii `mod` V.last bb == V.last bb - 1 = mul i (ii + 1) 0.0 (V.snoc f (val + ((aa V.! (ii `mod` V.last bb + (i * V.last bb))) * (aa2 V.! ii))))
            | otherwise = mul i (ii + 1) (val + ((aa V.! (ii `mod` V.last bb + (i * V.last bb))) * (aa2 V.! ii))) f
        -- mul :: Int -> Int -> Double -> V.Vector Double
        -- mul !i !ii !val
        --     | i == quot (V.product bb) (V.last bb) = V.empty
        --     | ii == V.product bb2 = mul (i + 1) 0 0.0 --(val : f)
        --     | ii `mod` V.last bb == V.last bb - 1 = V.snoc (mul i (ii + 1) 0.0) (val + ((aa V.! (ii `mod` V.last bb + (i * V.last bb))) * (aa2 V.! ii)))
        --     | otherwise = mul i (ii + 1) (val + ((aa V.! (ii `mod` V.last bb + (i * V.last bb))) * (aa2 V.! ii)))
        
dim :: D -> D -> [Int]
dim (D a d1) (D b d2) = take (m_1 $ length d1) d1 ++ take (m_1 $ m_1 $ length d2) d2 ++ take 1 (reverse d2)
    where 
        m_1 :: Int -> Int
        m_1 m = m - 1

verifyMul :: D -> D -> Bool
verifyMul (D _ a) (D _ b)
    | last a == b !! (length b - 2) = True
    | otherwise = False


verifyMulVector :: DeDa -> DeDa -> Bool
verifyMulVector (DeDa _ a) (DeDa _ b)
    | V.last a == b V.! (V.length b - 2) = True
    | otherwise = False


deepTranspose :: D -> D
deepTranspose (D a b) = D (reverse $ tsp $ length a - 1) (reverse b)
    where
        tsp :: Int -> [Double]
        tsp (-1) = []
        tsp i = a !! eq fs i : tsp (i - 1)

        fs :: [F]
        fs = mf b (length b)

        eq :: [F] -> Int -> Int
        eq [] _ = 0
        eq (F a b c : xs) i = ((quot i (product a) `mod` b) * product c) + eq xs i
        
        mf :: [Int] -> Int -> [F]
        mf _ 0 = []
        mf xs i = (\(x : ss) -> F (reverse ss) x ltake) ftake : mf xs (i - 1)
            where 
                ftake,ltake :: [Int]
                ftake = reverse $ take i xs
                ltake = reverse $ take (length xs - i) (reverse xs)
                -- l2 :: Int -> Double -> Double
                -- l2 ii f
                --     | ii == length (\(DeDa aa _) -> aa) a = f
                --     | i `mod` V.last $ (\(DeDa aa _) -> aa) a = 
        


        
test :: D
test = newDeepMatrix [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17] (3 --> 2 --> [3]) 
        *** newDeepMatrix [0,1,2,3,4,5,6,7,8,9,10,11] (2 --> 3 --> [2])

(-->) :: Int -> [Int] -> [Int]
a --> b = a : b
-- (-->) :: Int -> Double -> [Int]
-- a --> b = a : [b]

(<->) :: D -> D -> D
(D a1 b1) <-> (D a2 b2)
    | D a1 b1 == D [] [] = D a2 b2
    | D a2 b2 == D [] [] = D a1 b1
    | b1 == b2 = D (a1 ++ a2) ((\(x:xs) (y:_) -> x + y : xs) b1 b2)
    |  (\(_ :xs) -> xs) b1 == (\(_ :xs) -> xs) b2 = D (a1 ++ a2) ((\(x :xs) (y : _) ->  x + y : xs) b1 b2)
    -- | b2 == (\(_ :xs) -> xs) b1 = D (a1 ++ a2) ((\(x :xs) ->  x + 1 : xs) b1)



indexPull :: Int -> D -> D
indexPull i (D a b) = D (run 0) ((\(_ : xs) -> 1 : xs) b)
    where
        size = product $ (\(_:xs) -> xs) b
        arr = V.fromList a :: V.Vector Double
        run :: Int -> [Double]
        run step
            | step == size = []
            | otherwise = arr V.! ((i * (size)) + step) : run (step + 1)

-- indexPull 2 (D [1,2,3,4,5,6] [3,2])



