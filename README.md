# deepwater

## A modular Haskell deep learning framework made purely in Haskell  

## Experimental and early stages of development 

### Simple Example

```haskell
x = newDeepMatrix [0, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1] [4, 3]
y = newDeepMatrix [0, 1, 1, 0] [4, 1]

layers :: [Layers]
layers =
  [ neuralLayer 5 (makeStrdUpgrades sigmoid)
  , neuralLayer 8 (makeStrdUpgrades sigmoid)
  , neuralLayer 1 (makeStrdUpgrades sigmoid)
  ]

deepLearningTest = deepLearner x y layers 1000 4 differnce 45441
```