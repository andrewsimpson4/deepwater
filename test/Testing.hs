{-# LANGUAGE AllowAmbiguousTypes   #-}
{-# LANGUAGE GADTs                 #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE ParallelListComp      #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE StandaloneDeriving    #-}
{-# LANGUAGE TypeSynonymInstances  #-}

import           Control.Exception
import           Data.Array
import qualified Data.Text.IO                              as T
import           Data.Time
import qualified Data.Vector                               as V
import           DeepData.DeepData                         as D
import           System.IO
import           System.Random
import           Text.Printf

import           ActivationFunctions
import           Base                                      as B
import           Control.DeepSeq
import           General
import           Layers.FullyConnectedLayer

import qualified Data.ByteString.Lazy                      as BL
import           Data.Csv

import           BaseGPU

import           DeepData.Gpu.DeepGpu
import           Metrics.Metrics

import           LossFunctions

import           Layers.NeuralLayer.NeuralLayer

main :: IO ()
main = timeIt

xx :: X
yy :: Y
xx = newDeepMatrix [0, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1] [4, 3]

yy = newDeepMatrix [0, 1, 1, 0] [4, 1]

-- xx :: X
-- yy :: Y
-- xx = newDeepMatrix [0,
--                     2,
--                     2,
--                     1] [4,1]
-- yy = newDeepMatrix [0,3,3,2] [4,1]
layers2 :: [Layers]
layers2 =
  [ neuralLayer 5 (makeStrdUpgrades sigmoid)
  , neuralLayer 8 (makeStdBiasUpgrades sigmoid)
  , neuralLayer 1 (makeStrdUpgrades sigmoid)
  ]
           -- fullyConnected 1 sigmoid]
            -- fullyConnected 10 sigmoid]

-- layers2 :: [Layers]
-- layers2 = [ fullyConnected 3 sigmoid,
--             fullyConnected 4 sigmoid,
--             fullyConnected 1 sigmoid]
-- layers2 :: [Layers]
-- layers2 = [ dropout 4 sigmoid,
--             dropout 1 sigmoid]
dt = deepLearner xx yy layers2 50 3 differnce 4535441

dtGPU = deepLearnerGPU xx yy layers2 100 4535441

-- xSpeed1 = newDeepMatrix [0,1,2,3,4,5,6,7,8,9,10,11] [2,2,3]
-- xSpeed2 = newDeepMatrix [0,1,2,3,4,5,6,7,8,9,10,11] [2,3,2]
xSpeed1 = newDeepRandomMatrix [300, 13] 76542

xSpeed2 = newDeepRandomMatrix [13, 100] 7657654

timeIt :: IO ()
timeIt
    -- let x = newDeepMatrix [2,3,4,2,3,4,2,3,4,0] [10,1]
    -- let y = newDeepMatrix [1,2,3,1,2,3,1,2,3,0] [10,1]
    -- let scX = newDeepMatrix (tanhNorm 20 (matrix x)) (shape x)
    -- -- let scY = newDeepMatrix (sigNorm 4 (matrix y)) (shape y)
    -- let sc = newDeepMatrix (sigNorm 1 (matrix y)) (shape y)
    -- -- print scX
    -- print sc
 = do
  start <- getCurrentTime
    -- hX <- openFile "heartX.ddml" ReadMode
    -- cX <- hGetContents hX
    -- let x = read cX :: D
    --let normX = tanhNorm 4 (matrix x)
    --let scX = newDeepMatrix (fst normX) (shape x)
    --print scX
    -- --hClose hX
    -- hY <- openFile "heartY.ddml" ReadMode
    -- cY <- hGetContents hY
    -- let y = read cY :: D
    --let norm = sigNorm 1 (matrix y)
    --let sc = newDeepMatrix (fst norm) (shape y)
    --print sc
    --hClose hY
  let dt = deepLearner xx yy layers2 0 4 differnce 45441
  print dt
   -- print dt
   --  dt <- deepLearnerGPU x y layers2 10 4535441
    -- print dt
  -- print "PREDICTION"
    -- let y1 = unSigNorm 1 (snd norm) (matrix $ predict scX dt)
  -- let y1 = predict xx dt
    --let unS = unSigNorm 1 (snd norm) (matrix y1)
    -- print sc
  -- print y1
    -- print $ (sum $ matrix y1) / (fromIntegral $ product $ shape y1 :: Double)
    --print unS
  -- print "mean abs score: "
  -- print $ meanAbsoluteError yy y1
    -- let func = (\(LossFunction f) -> f) crossEntropyLoss
    -- print $ func yy y1
    -- print "starting time"
    -- start <- getCurrentTime
    -- x1 <- dtGPU
    -- print x1
    -- x1 <- mmGpu xSpeed1 xSpeed2
    -- print x1
    -- let !xx = x1
    -- print $ last x1
    -- snd <- getCurrentTime
    -- print (diffUTCTime snd start)
    -- let x1 = xSpeed1 *** xSpeed2
    -- print x1
    -- x2 <- xSpeed1 <***> xSpeed2
    -- let xx2 = x2
    -- print x2
    -- let x1 = length $ matrix $ xSpeed1 *** xSpeed2
    -- print $ takeSampleTo 1 x1
    -- print $ takeSampleLast  x1
    -- let !x1 = dt
    -- print x1
    -- print "PREDICTION"
    -- let !y1 = predict xx x1
    -- print y1
    -- print $ predict x1 xx sigmoid
    -- xx2 <- mainGPU
    -- xx2 <- runSum
  end <- dt `deepseq` getCurrentTime
  print (diffUTCTime end start)
    -- print $ loadFile "heart.csv"
    -- csvData <- BL.readFile "heart.csv"
    -- case decode NoHeader csvData of
    --             Left err -> putStrLn err
    --             Right v -> V.forM_ v $ \ (a1 :: String,a2 :: Int,a3 :: Int,a4 :: Int,a5 :: Int,a6 :: Int,a7 :: Int,a8 :: Int,a9 :: Int,a10 :: Int,a11 :: Int,a12 :: Int,a13 :: Int,a14 :: Int) ->
    --                 putStrLn a1
-- xx2,yy2 :: D.D
-- xx2 = newDeepMatrix [20,54,3,5] [2,2]
-- yy2 = newDeepMatrix [3,6,8,3] [2,2]
-- yt = newDeepMatrix [1,1,1,1] [4,1]
-- w1,w2 :: D.D
-- w1 = newDeepRandomMatrix [3,4] 42543
-- w2 = newDeepRandomMatrix [4,1] 76542
-- speedTest = newDeepRandomMatrix [10,10,10,10,2] 76542
-- speedTest2 = newDeepRandomMatrix [10,10,10,2,10] 7657654
-- speedTest3 = matrix $ newDeepRandomMatrix [10,10,10,10,10,10,10,10] 7657665
-- loadFile :: FromRecord a => String -> IO (V.Vector a)
-- loadFile s = do
--     csvData <- BL.readFile s
--     case decode NoHeader csvData of
--                 Left err -> error "cant read"
--                 Right v -> return v
-- data Person = Person
--     { age   :: !Int
--     , sex :: !Int
--     , cp :: !Int
--     , trestbps :: !Int
--     , chol :: !Int
--     , fbs :: !Int
--     , restecg :: !Int
--     , thalach :: !Int
--     , exang :: !Int
--     , slope :: !Int
--     , ca :: !Int
--     , thal :: !Int
--     , target :: !Int
--     }
-- instance FromNamedRecord Person where
--     parseNamedRecord r = Person <$> r .: "age" <*> r .: "sex" <*> r .: "cp"
--                                 <*> r .: "trestbps"  <*> r .: "chol"  <*> r .: "fbs"  <*> r .: "restecg"
--                                 <*> r .: "thalach" <*> r .: "exang" <*> r .: "slope" <*> r .: "ca"
--                                 <*> r .: "thal" <*> r .: "target"
